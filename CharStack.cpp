#include "CharStack.h"
#include <iostream>

using namespace std;

    CharStack::CharStack(int max) {
        // TODO
        stack = new char[max];
        maxSize=max;
    }
    CharStack::~CharStack() {
        delete [] stack;
    }

bool CharStack::push(char c){//false if pushing exceeds the max
        if (size>=maxSize){
            return false;
        } else {
        stack[size]=c;
        size++;
        return true;
        }
    }

char CharStack::pop() {//0 if stack is empty
        char returnChar;
        if (isEmpty()){
            return 0;
        }else {
            size--;
            returnChar=stack[size];
            stack[size]=' ';
            return returnChar;
        }
    }

bool CharStack::isEmpty() const {
        if (size==0){
            return true;
        }else {
            return false;
        }
    }
