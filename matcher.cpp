#include <iostream>

using namespace std;

class CharStack {
private:
    int maxSize;
    char *stack;
    int size=0;
public:
    CharStack(int max) {
        // TODO
        stack = new char[max];
        maxSize=max;
    }
    ~CharStack() {
        delete [] stack;
    }
    bool push(char c) {//false if pushing exceeds the max
        if (size>=maxSize){
            return false;
        } else {
        stack[size]=c;
        size++;
        return true;
        }
    }
    char pop() {//0 if stack is empty
        char returnChar;
        if (size==0){
            return 0;
        }else {
            size--;
            returnChar=stack[size];
            stack[size]=' ';
            return returnChar;
        }
    }
    bool isEmpty() const {
        if (size==0){
            return true;
        }else {
            return false;
        }
    }
};

class PairMatcher {
private:
    char _openChar, _closeChar;
    CharStack charStack;
public:
    PairMatcher(char openChar, char closeChar) : charStack(100) {
        _openChar = openChar;
        _closeChar = closeChar;
    }
    bool check(const string &testString) {
        for (int i = 0; i < testString.length(); i++) {
            if (testString[i] == _openChar) {
                charStack.push(_openChar);
            } else if (testString[i] == _closeChar) {
                if (charStack.pop() != _openChar) {
                    return false;
                }
            }
        }
        return charStack.isEmpty();
    }
};

int main() {
    PairMatcher matcher('(', ')');

    string testString = "(((())))";

    cout << testString << " is " << (matcher.check(testString) ? "valid" : "invalid") << endl;
    return 0;
}
