#ifndef PAIRMATCHER_H_INCLUDED
#define PAIRMATCHER_H_INCLUDED
#include "CharStack.h"
#include <string>


class PairMatcher {
private:
    char _openChar, _closeChar;
    CharStack charStack;
public:
    PairMatcher(char openChar, char closeChar);
    bool check(const std::string &testString);
};

#endif // PAIRMATCHER_H_INCLUDED
