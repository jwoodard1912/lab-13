#ifndef CHARSTACK_H_INCLUDED
#define CHARSTACK_H_INCLUDED

class CharStack {
private:
    int maxSize;
    char *stack;
    int size=0;
public:
    CharStack(int max);
    ~CharStack();
    bool push(char c);
    char pop();
    bool isEmpty() const;
};

#endif // CHARSTACK_H_INCLUDED
